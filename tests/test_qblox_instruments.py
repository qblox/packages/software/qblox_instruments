# ----------------------------------------------------------------------------
# Description    : Qblox Instruments test script
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------


# -- include -----------------------------------------------------------------

from datetime import datetime

import fastjsonschema
import pytest

from qblox_instruments import (
    BuildInfo,
    Cluster,
    ClusterType,
    DeviceInfo,
    InstrumentType,
    SequencerStates,
    SequencerStatus,
    SequencerStatuses,
    SequencerStatusFlags,
    SystemStatus,
    SystemStatuses,
    SystemStatusFlags,
    SystemStatusSlotFlags,
    __version__,
    get_build_info,
)

# -- definitions -------------------------------------------------------------

DUMMY_CFG = {
    "1": ClusterType.CLUSTER_QCM,
    "3": ClusterType.CLUSTER_QCM,
    "4": ClusterType.CLUSTER_QCM_RF,
    "8": ClusterType.CLUSTER_QCM_RF,
    "10": ClusterType.CLUSTER_QDM,
    "11": ClusterType.CLUSTER_QTM,
    "15": ClusterType.CLUSTER_QRM,
    "16": ClusterType.CLUSTER_QRM_RF,
    "20": ClusterType.CLUSTER_QCM_RF,
}


# -- fixtures ----------------------------------------------------------------


@pytest.fixture(name="cluster")
def make_dummy_cluster():
    clstr = Cluster("cluster", dummy_cfg=DUMMY_CFG)
    yield clstr

    # Clean up when done
    clstr.close()


# -- functions ---------------------------------------------------------------


def test_device_info(cluster):
    """
    Tests DeviceInfo object methods.
    """

    # Create device info from dummy
    device_info = DeviceInfo.from_idn(cluster._get_idn())

    # Check device info contents
    assert device_info.manufacturer == "qblox"
    assert device_info.model == "cluster_mm"
    assert device_info.serial == "whatever"

    assert "fw" in device_info
    assert "kmod" in device_info
    assert "sw" in device_info
    assert "cfg_man" not in device_info

    # Replace build timestamps for conversion to unix timestamp to work.
    # The dummy timestamps are before 1970 and so will fail.
    timestamp = datetime.strptime("01/01/2000-00:00:00", "%d/%m/%Y-%H:%M:%S")
    device_info._fw_build._build = timestamp
    device_info._kmod_build._build = timestamp
    device_info.sw_build._build = timestamp

    # Get build info objects
    fw_build_info = device_info.fw_build
    assert fw_build_info is not None
    kmod_build_info = device_info.kmod_build
    assert kmod_build_info is not None
    sw_build_info = device_info.sw_build
    assert sw_build_info is not None
    cfg_man_build_info = device_info.cfg_man_build
    assert cfg_man_build_info is None

    # Check device info IDN string
    assert device_info.to_idn() == "{},{},{},{} {} {}".format(
        device_info.manufacturer,
        device_info.model,
        device_info.serial,
        sw_build_info.to_idn("sw"),
        fw_build_info.to_idn("fw"),
        kmod_build_info.to_idn("kmod"),
    )

    # Check device info IDN dict
    device_info_dict = {
        "manufacturer": device_info.manufacturer,
        "model": device_info.model,
        "ser": device_info.serial,
        "fw": fw_build_info.to_dict(),
        "kmod": kmod_build_info.to_dict(),
        "sw": sw_build_info.to_dict(),
        "is_extended_instrument": False,
        "is_rf": False,
    }
    assert device_info.to_dict() == device_info_dict
    assert device_info == DeviceInfo.from_dict(device_info_dict)

    # Check device info IDN tuple
    assert device_info.to_tuple() == (
        device_info.manufacturer,
        device_info.model,
        device_info.name,
        device_info.serial,
        device_info.is_extended_instrument,
        device_info.is_rf,
        sw_build_info.to_tuple(),
        fw_build_info.to_tuple(),
        kmod_build_info.to_tuple(),
        None,
        None,
    )


# ----------------------------------------------------------------------------
def test_build_info(cluster):
    """
    Tests BuildInfo object methods.
    """

    # Create device info from dummy
    build_info = DeviceInfo.from_idn(cluster._get_idn()).fw_build

    # Replace build timestamp for conversion to unix timestamp to work.
    # The dummy timestamps are before 1970 and so will fail.
    timestamp = datetime.strptime("01/01/2000-00:00:00", "%d/%m/%Y-%H:%M:%S")
    build_info._build = timestamp

    # Check build info contents
    assert build_info.version == (0, 0, 0)
    assert build_info.build == timestamp
    assert build_info.build_iso == timestamp.isoformat()
    assert build_info.build_unix == timestamp.timestamp()
    assert build_info.hash == int("0xDEADBEAF", 16)
    assert build_info.hash_str == "DEADBEAF".lower()
    assert build_info.dirty is False
    assert build_info.dirty_str == "0"

    # Check build info IDN string
    assert build_info.to_idn() == (
        f"Version={build_info.version_str} "
        f"Build={build_info.build_str} "
        f"Hash=0x{build_info.hash:08X} "
        f"Dirty={build_info.dirty_str}"
    )

    # Check build info dict
    build_info_dict = {
        "version": build_info.version,
        "build": build_info.build_unix,
        "hash": build_info.hash,
        "dirty": build_info.dirty,
    }
    assert build_info.to_dict() == build_info_dict
    assert build_info == BuildInfo.from_dict(build_info_dict)

    # Check build info tuple
    assert build_info.to_tuple() == (
        build_info.version,
        build_info.build_unix,
        build_info.hash,
        build_info.dirty,
    )


# ----------------------------------------------------------------------------
def test_get_build_info():
    """
    Tests get build info function and checks if the returned dictionary has the
    correct format. If not, the test fails.
    """

    # Build info
    build_info = get_build_info()

    # Check build info
    build_info_schema = {
        "title": "Build information container.",
        "description": "Contains build information.",
        "required": ["version", "build", "hash", "dirty"],
        "properties": {
            "version": {"description": "Version string", "type": "string"},
            "build": {"description": "Build date", "type": "string"},
            "hash": {"description": "Git hash", "type": "string"},
            "dirty": {"description": "Git dirty indication", "type": "boolean"},
        },
    }

    validate_build_info = fastjsonschema.compile(build_info_schema)
    validate_build_info(build_info.to_idn_dict())


# ----------------------------------------------------------------------------
def test_version():
    """
    Test if __version__ matches version in the build information else fail.
    """

    # Test version
    assert __version__ == get_build_info().version_str


# ----------------------------------------------------------------------------
def test_instrument_types():
    """
    Test instrument type string representations.
    """

    assert repr(InstrumentType.QCM) == "<InstrumentType.QCM>"
    assert str(InstrumentType.QCM) == "QCM"
    assert InstrumentType.QCM == "QCM"
    assert list({InstrumentType.QCM: ""}.keys()) == ["QCM"]

    assert repr(ClusterType.CLUSTER_QRM_RF) == "<ClusterType.CLUSTER_QRM_RF>"
    assert str(ClusterType.CLUSTER_QRM_RF) == "Cluster QRM-RF"
    assert ClusterType.CLUSTER_QRM_RF == "Cluster QRM-RF"
    assert list({ClusterType.CLUSTER_QRM_RF: ""}.keys()) == ["Cluster QRM-RF"]


# ----------------------------------------------------------------------------
def test_system_status():
    """
    Test system status string representations.
    """

    # Test system state string representations
    state = SystemStatus(SystemStatuses.OKAY, [], SystemStatusSlotFlags())
    assert repr(state) == (
        "SystemStatus(status=<SystemStatuses.OKAY>, flags=[], slot_flags=SystemStatusSlotFlags())"
    )
    assert str(state) == "Status: OKAY, Flags: NONE, Slot flags: NONE"

    state = SystemStatus(
        SystemStatuses.OKAY,
        [SystemStatusFlags.PLL_UNLOCKED],
        SystemStatusSlotFlags(
            {
                "slot1": [SystemStatusFlags.PLL_UNLOCKED],
                "slot20": [SystemStatusFlags.PLL_UNLOCKED],
            }
        ),
    )
    assert repr(state) == (
        "SystemStatus(status=<SystemStatuses.OKAY>, "
        "flags=[<SystemStatusFlags.PLL_UNLOCKED>], "
        "slot_flags=SystemStatusSlotFlags("
        "slot1=[<SystemStatusFlags.PLL_UNLOCKED>], "
        "slot20=[<SystemStatusFlags.PLL_UNLOCKED>]))"
    )
    assert str(state) == (
        "Status: OKAY, Flags: PLL_UNLOCKED, Slot flags: SLOT1_PLL_UNLOCKED, SLOT20_PLL_UNLOCKED"
    )

    assert SystemStatuses.OKAY == "OKAY"
    assert list({SystemStatuses.OKAY: True}.keys()) == ["OKAY"]

    assert SystemStatusFlags.PLL_UNLOCKED == "PLL_UNLOCKED"
    assert list({SystemStatusFlags.PLL_UNLOCKED: ""}.keys()) == ["PLL_UNLOCKED"]


# ----------------------------------------------------------------------------
def test_sequencer_status():
    """
    Test sequencer status string representations.
    """

    # Test sequencer state string representations
    status = SequencerStatus(SequencerStatuses.OKAY, SequencerStates.IDLE, [], [], [], [])
    assert repr(status) == (
        "SequencerStatus(status=<SequencerStatuses.OKAY>, "
        "state=<SequencerStates.IDLE>, "
        "info_flags=[], "
        "warn_flags=[], "
        "err_flags=[], "
        "log=[])"
    )
    assert str(status) == (
        "Status: OKAY, State: IDLE, Info Flags: NONE, Warning Flags: NONE, Error Flags: NONE, "
        "Log: []"
    )

    status = SequencerStatus(
        SequencerStatuses.WARNING,
        SequencerStates.Q1_STOPPED,
        [],
        [SequencerStatusFlags.TRIGGER_NETWORK_MISSED_INTERNAL_TRIGGER],
        [],
        ["missed_tgr_addr:_7"],
    )
    assert repr(status) == (
        "SequencerStatus(status=<SequencerStatuses.WARNING>, "
        "state=<SequencerStates.Q1_STOPPED>, "
        "info_flags=[], "
        "warn_flags=[<SequencerStatusFlags.TRIGGER_NETWORK_MISSED_INTERNAL_TRIGGER>], "
        "err_flags=[], "
        "log=['missed_tgr_addr:_7'])"
    )

    assert str(status) == (
        "Status: WARNING, State: Q1_STOPPED, "
        "Info Flags: NONE, "
        "Warning Flags: TRIGGER_NETWORK_MISSED_INTERNAL_TRIGGER, "
        "Error Flags: NONE, "
        "Log: ['missed_tgr_addr:_7']"
    )

    status = SequencerStatus(
        SequencerStatuses.OKAY,
        SequencerStates.Q1_STOPPED,
        [
            SequencerStatusFlags.ACQ_SCOPE_DONE_PATH_0,
            SequencerStatusFlags.ACQ_SCOPE_DONE_PATH_1,
        ],
        [],
        [],
        [],
    )
    assert repr(status) == (
        "SequencerStatus(status=<SequencerStatuses.OKAY>, "
        "state=<SequencerStates.Q1_STOPPED>, "
        "info_flags=[<SequencerStatusFlags.ACQ_SCOPE_DONE_PATH_0>, "
        "<SequencerStatusFlags.ACQ_SCOPE_DONE_PATH_1>], "
        "warn_flags=[], "
        "err_flags=[], "
        "log=[])"
    )

    assert str(status) == (
        "Status: OKAY, "
        "State: Q1_STOPPED, "
        "Info Flags: ACQ_SCOPE_DONE_PATH_0, ACQ_SCOPE_DONE_PATH_1, "
        "Warning Flags: NONE, "
        "Error Flags: NONE, "
        "Log: []"
    )

    status = SequencerStatus(
        SequencerStatuses.ERROR,
        SequencerStates.Q1_STOPPED,
        [],
        [],
        [SequencerStatusFlags.SEQUENCE_PROCESSOR_Q1_ILLEGAL_INSTRUCTION],
        [],
    )
    assert repr(status) == (
        "SequencerStatus(status=<SequencerStatuses.ERROR>, "
        "state=<SequencerStates.Q1_STOPPED>, "
        "info_flags=[], "
        "warn_flags=[], "
        "err_flags=[<SequencerStatusFlags.SEQUENCE_PROCESSOR_Q1_ILLEGAL_INSTRUCTION>], "
        "log=[])"
    )

    assert str(status) == (
        "Status: ERROR, "
        "State: Q1_STOPPED, "
        "Info Flags: NONE, "
        "Warning Flags: NONE, "
        "Error Flags: SEQUENCE_PROCESSOR_Q1_ILLEGAL_INSTRUCTION, "
        "Log: []"
    )


# ----------------------------------------------------------------------------
def test_sequencer_status_types():
    """
    Test sequencer status types string representations.
    """
    assert SequencerStatuses.OKAY == "OKAY"
    assert SequencerStatuses.WARNING == "WARNING"
    assert SequencerStatuses.ERROR == "ERROR"

    assert list({SequencerStatuses.OKAY: ""}.keys()) == ["OKAY"]
    assert list({SequencerStatuses.WARNING: ""}.keys()) == ["WARNING"]
    assert list({SequencerStatuses.ERROR: ""}.keys()) == ["ERROR"]

    assert SequencerStates.IDLE == "IDLE"
    assert SequencerStates.ARMED == "ARMED"
    assert SequencerStates.RUNNING == "RUNNING"
    assert SequencerStates.Q1_STOPPED == "Q1_STOPPED"
    assert SequencerStates.STOPPED == "STOPPED"

    assert list({SequencerStates.IDLE: ""}.keys()) == ["IDLE"]
    assert list({SequencerStates.ARMED: ""}.keys()) == ["ARMED"]
    assert list({SequencerStates.RUNNING: ""}.keys()) == ["RUNNING"]
    assert list({SequencerStates.Q1_STOPPED: ""}.keys()) == ["Q1_STOPPED"]
    assert list({SequencerStates.STOPPED: ""}.keys()) == ["STOPPED"]
