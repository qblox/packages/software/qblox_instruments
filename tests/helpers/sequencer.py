# ----------------------------------------------------------------------------
# Description    : Sequencer test functions
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------


# -- include -----------------------------------------------------------------

import copy
import json
import math
import os
import re
import struct
import sys
from math import isnan

import pytest
import scipy.signal

from qblox_instruments import (
    DummyBinnedAcquisitionData,
    DummyScopeAcquisitionData,
    SequencerStates,
    SequencerStatus,
    SequencerStatuses,
    SequencerStatusFlags,
)


# ----------------------------------------------------------------------------
def test_channelmap(instrument, num_outputs, num_inputs, is_rf):
    # Test the QCoDeS parameters.
    for seq in instrument.sequencers:
        if num_outputs:
            test_channelmap_awg(seq, num_outputs, is_rf)
        if num_inputs:
            test_channelmap_acq(seq, num_inputs, is_rf)

    def get_all(io_filter=""):
        return sorted(filter(lambda x: io_filter in x[2], instrument._iter_connections()))

    # Test the disconnect_outputs() utility function.
    if num_outputs:
        if is_rf:
            instrument.sequencer0.connect_out0("IQ")
        else:
            instrument.sequencer0.connect_out0("I")
        assert get_all("dac")
        instrument.disconnect_outputs()
        assert not get_all("dac")

    # Test the disconnect_inputs() utility function.
    if num_inputs:
        if is_rf:
            instrument.sequencer0.connect_acq("in0")
        else:
            instrument.sequencer0.connect_acq_I()
        assert get_all("adc")
        instrument.disconnect_inputs()
        assert not get_all("adc")

    # Test the connect utility function.
    if num_outputs:
        if not is_rf:
            with pytest.raises(ValueError, match="syntax error"):
                instrument.sequencer0.connect_sequencer("out0", "garbage")
            with pytest.raises(
                ValueError, match="cannot connect I and Q path to the same I/O port"
            ):
                instrument.sequencer0.connect_sequencer("out0", "out0_0")
            assert get_all("dac") == []

            instrument.sequencer0.connect_sequencer("out0")
            assert get_all("dac") == [(0, "I", "dac0")]

            with pytest.raises(RuntimeError, match="connection already exists"):
                instrument.sequencer0.connect_sequencer("out0_1")
            assert get_all("dac") == [(0, "I", "dac0")]

            with pytest.raises(RuntimeError, match="already connected to the other I/Q path"):
                instrument.sequencer0.connect_sequencer("out1_0")
            assert get_all("dac") == [(0, "I", "dac0")]

            if num_outputs >= 3:
                instrument.disconnect_outputs()
                instrument.sequencer0.connect_sequencer("out0_1", "out2")
                assert get_all("dac") == [
                    (0, "I", "dac0"),
                    (0, "I", "dac2"),
                    (0, "Q", "dac1"),
                ]

        else:
            instrument.sequencer0.connect_sequencer("out0")
            assert get_all("dac") == [(0, "I", "dac0"), (0, "Q", "dac1")]

            with pytest.raises(ValueError, match="did you mean 'out1'"):
                instrument.sequencer0.connect_sequencer("out2_3")
            assert get_all("dac") == [(0, "I", "dac0"), (0, "Q", "dac1")]

    if num_inputs and num_outputs:
        if not is_rf:
            instrument.disconnect_inputs()
            instrument.disconnect_outputs()
            instrument.sequencer0.connect_sequencer("io0")
            assert get_all() == [(0, "I", "dac0"), (0, "acq_I", "adc0")]

            instrument.disconnect_inputs()
            instrument.disconnect_outputs()
            instrument.sequencer0.connect_sequencer("io0_1")
            assert get_all() == [
                (0, "I", "dac0"),
                (0, "Q", "dac1"),
                (0, "acq_I", "adc0"),
                (0, "acq_Q", "adc1"),
            ]

        else:
            instrument.disconnect_inputs()
            instrument.disconnect_outputs()
            instrument.sequencer0.connect_sequencer("io0")
            assert get_all() == [
                (0, "I", "dac0"),
                (0, "Q", "dac1"),
                (0, "acq_I", "adc0"),
                (0, "acq_Q", "adc1"),
            ]

    if num_inputs:
        if not is_rf:
            instrument.disconnect_inputs()
            instrument.sequencer0.connect_sequencer("in0")
            assert get_all("adc") == [(0, "acq_I", "adc0")]

            instrument.disconnect_inputs()
            instrument.sequencer0.connect_sequencer("in0_1")
            assert get_all("adc") == [(0, "acq_I", "adc0"), (0, "acq_Q", "adc1")]

        else:
            instrument.disconnect_inputs()
            instrument.sequencer0.connect_sequencer("in0")
            assert get_all("adc") == [(0, "acq_I", "adc0"), (0, "acq_Q", "adc1")]


# ----------------------------------------------------------------------------
def test_channelmap_awg(sequencer, num_outputs, is_rf):
    """
    Tests waveform gen channel map setting and getting function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    num_outputs
        Number of outputs
    if_rf:
        Whether the outputs are RF or baseband
    """
    if is_rf:
        states = {
            "off": "off",
            "IQ": "IQ",
            False: "off",
            True: "IQ",
        }
    else:
        states = {
            "off": "off",
            "I": "I",
            "Q": "Q",
        }

    for output in range(num_outputs):
        for alias, canonical in states.items():
            sequencer.parameters[f"connect_out{output}"].set(alias)
            assert sequencer.parameters[f"connect_out{output}"].get() == canonical


# ----------------------------------------------------------------------------
def test_channelmap_acq(sequencer, num_inputs, is_rf):
    """
    Tests acquisition channel map setting and getting function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    num_inputs
        Number of inputs
    if_rf:
        Whether the inputs are RF or baseband
    """
    states = {"off": "off"}
    for input in range(num_inputs):
        state = f"in{input}"
        states[state] = state
    if num_inputs == 1:
        states[False] = "off"
        states[True] = "in0"

    parameters = ["connect_acq"] if is_rf else ["connect_acq_I", "connect_acq_Q"]

    for parameter in parameters:
        for alias, canonical in states.items():
            sequencer.parameters[parameter].set(alias)
            assert sequencer.parameters[parameter].get() == canonical


# ----------------------------------------------------------------------------
def test_waveform_weight_handling(sequencer, type):
    """
    Tests waveform or weight handling (e.g. adding, deleting) function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    type
        Waveforms or weights
    """

    # Get waveforms/weights attributes
    get_wave_weights = getattr(sequencer, f"get_{type}s")

    # Check if waveform or weight lists is empty
    assert get_wave_weights() == {}

    # Generate waveforms or weights and create sequence
    waveform_length = 100
    waveforms = {
        "gaussian": {
            "data": scipy.signal.windows.gaussian(
                waveform_length, std=0.12 * waveform_length
            ).tolist()
        },  # Will automatically be placed at index 0
        "sine": {
            "data": [
                math.sin((2 * math.pi / waveform_length) * i) for i in range(0, waveform_length)
            ],
            "index": 1,
        },
        "sawtooth": {
            "data": [(1.0 / (waveform_length)) * i for i in range(0, waveform_length)],
            "index": 2,
        },
        "block": {"data": [1.0 for i in range(0, waveform_length)], "index": 3},
    }
    sequence = {
        "waveforms": copy.deepcopy(waveforms),
        "weights": copy.deepcopy(waveforms),
        "acquisitions": {},
        "program": "stop",
    }
    waveforms["gaussian"]["index"] = 0  # Add index for referencing
    sequencer.sequence(sequence)

    # Get waveforms or weights from sequencer and check data and indexes
    waveforms_out = get_wave_weights()
    for name in waveforms:
        assert waveforms[name]["index"] == waveforms_out[name]["index"]
        for sample0, sample1 in zip(waveforms[name]["data"], waveforms_out[name]["data"]):
            assert struct.unpack("f", struct.pack("f", sample0))[0] == sample1

    # Delete waveforms or weights from sequence
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": {},
        "program": "stop",
    }
    sequencer.sequence(sequence)

    # Check if waveform or weight lists is empty again
    assert get_wave_weights() == {}


# ----------------------------------------------------------------------------
def test_acquisition_handling(sequencer):
    """
    Tests waveform handling (e.g. adding, deleting) function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    """

    # Check if acquisition list is empty
    assert sequencer.get_acquisitions() == {}

    # Define acquisitions and create sequence
    acquisitions = {
        "acq0": {"num_bins": 10},  # Will automatically be placed at index 0
        "acq1": {"num_bins": 20, "index": 1},
        "acq2": {"num_bins": 30, "index": 2},
        "acq3": {"num_bins": 40, "index": 3},
    }
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": copy.deepcopy(acquisitions),
        "program": "stop",
    }
    acquisitions["acq0"]["index"] = 0  # Add index for referencing
    sequencer.sequence(sequence)

    # Store scope acquisitions and retrieve acquisitions
    for name in acquisitions:
        sequencer.store_scope_acquisition(name)
    acq_out = sequencer.get_acquisitions()

    # Check acquisition content
    sample_width = 12
    max_sample_value = 2 ** (sample_width - 1) - 1
    size = 2**14
    scope_acq0 = struct.unpack(
        "i" * size,
        struct.pack("i" * size, *[int(max_sample_value / size) * i for i in range(0, size)]),
    )
    scope_acq1 = struct.unpack(
        "i" * size,
        struct.pack(
            "i" * size,
            *[max_sample_value - int(max_sample_value / size) * i for i in range(0, size)],
        ),
    )
    for name in acq_out:
        assert acq_out[name]["index"] == acquisitions[name]["index"]
        for sample0, sample1 in zip(
            scope_acq0, acq_out[name]["acquisition"]["scope"]["path0"]["data"]
        ):
            assert sample0 / max_sample_value == sample1
        for sample0, sample1 in zip(
            scope_acq1, acq_out[name]["acquisition"]["scope"]["path1"]["data"]
        ):
            assert sample0 / max_sample_value == sample1
        assert (
            len(acq_out[name]["acquisition"]["bins"]["integration"]["path0"])
            == acquisitions[name]["num_bins"]
        )
        assert (
            len(acq_out[name]["acquisition"]["bins"]["integration"]["path1"])
            == acquisitions[name]["num_bins"]
        )
        assert (
            len(acq_out[name]["acquisition"]["bins"]["threshold"]) == acquisitions[name]["num_bins"]
        )
        assert (
            len(acq_out[name]["acquisition"]["bins"]["avg_cnt"]) == acquisitions[name]["num_bins"]
        )

    # Test deleting acquisition data without deleting the acquisition
    for name in acq_out:
        acq_out = sequencer.get_acquisitions()
        for bin in range(len(acq_out[name]["acquisition"]["bins"]["integration"]["path0"]) - 1):
            assert str(acq_out[name]["acquisition"]["bins"]["integration"]["path0"][bin]) == "nan"
            assert str(acq_out[name]["acquisition"]["bins"]["integration"]["path1"][bin]) == "nan"
            assert str(acq_out[name]["acquisition"]["bins"]["threshold"][bin]) == "nan"
            assert acq_out[name]["acquisition"]["bins"]["avg_cnt"][bin] == 0
        break
        # Execute only once

    # Clear acquisitions from sequence
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": {},
        "program": "stop",
    }
    sequencer.delete_dummy_binned_acquisition_data()
    sequencer.sequence(sequence)
    sequencer.start_sequencer()

    # Check if acquisition list is empty again
    assert sequencer.get_acquisitions() == {}

    # Get acquisition status (dummy always returns True)
    assert sequencer.get_acquisition_status() is True


# ----------------------------------------------------------------------------
def test_program_handling(sequencer, tmpdir):
    """
    Tests program handling function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    tmpdir
        Temporary directory
    """

    # Program
    seq_prog = """
          move      1,R0        # Start at marker output channel 0 (move 1 into R0)
          nop                   # Wait a cycle for R0 to be available.

    loop: set_mrk   R0          # Set marker output channels to R0
          upd_param 1000        # Update marker output channels and wait 1us.
          asl       R0,1,R0     # Move to next marker output channel (left-shift R0).
          nop                   # Wait a cycle for R0 to be available.
          jlt       R0,16,@loop # Loop until all 4 marker output channels have been set once.

          set_mrk   0           # Reset marker output channels.
          upd_param 4           # Update marker output channels.
          stop                  # Stop sequencer.
    """

    # If no sequence has been uploaded yet, getting it should raise
    with pytest.raises(RuntimeError):
        sequencer.sequence.cache()

    # Upload program through dictionary
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": {},
        "program": seq_prog,
    }
    sequencer.sequence(sequence)

    # Get cached sequence
    with pytest.raises(RuntimeError):
        sequencer.sequence()
    assert sequencer.sequence.cache() == sequence

    # Upload program through JSON file
    with open(os.path.join(tmpdir, "sequence.json"), "w", encoding="utf-8") as file:
        json.dump(sequence, file, indent=4)
        file.close()
    sequencer.sequence(os.path.join(tmpdir, "sequence.json"))

    # Get assembler status and log
    assert sequencer.parent.get_assembler_status() is True
    assert (
        re.match(
            ".*assembler finished successfully.*",
            sequencer.parent.get_assembler_log(),
            re.DOTALL,
        )
        is not None
    )

    # Add illegal program (and waveforms) and upload
    # (suppress error log printed by exception)
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": {},
        "program": seq_prog + "\n random_instruction",
    }
    try:
        new_stdout = open(os.devnull, "w")  # noqa: SIM115
        sys.stdout = new_stdout
        sequencer.sequence(sequence)
        raise AssertionError("Programming sequence should have failed.")
    except RuntimeError:
        pass
    finally:
        sys.stdout = sys.__stdout__

    # Get assembler status and log
    assert sequencer.parent.get_assembler_status() is False
    assert (
        re.match(
            ".*assembler failed with .* errors:.*",
            sequencer.parent.get_assembler_log(),
            re.DOTALL,
        )
        is not None
    )


# ----------------------------------------------------------------------------
def test_sequencer_control(sequencer, is_qrm_type):
    """
    Tests sequencer handling function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    is_qrm_type
        Sequencer parent is of type QRM
    """

    # Reading and writing to a single parameter will trigger a read and
    # write of all sequencer settings.
    sequencer.nco_freq(10e6)
    assert sequencer.nco_freq() == 10e6

    # Also check discretazation phase rotation since that has some specific
    # functionality attached.
    if is_qrm_type:
        sequencer.thresholded_acq_rotation(270)
        assert sequencer.thresholded_acq_rotation() == 270

    # Check sequencer control
    sequencer.arm_sequencer()
    sequencer.start_sequencer()
    sequencer.stop_sequencer()

    # Check sequencer control to control all sequencers at once
    sequencer.root_instrument.arm_sequencer()
    sequencer.root_instrument.start_sequencer()
    sequencer.root_instrument.stop_sequencer()

    # Get sequencer status (dummy always returns STOPPED)
    state = SequencerStatus(
        SequencerStatuses.OKAY,
        SequencerStates.STOPPED,
        [SequencerStatusFlags.ACQ_BINNING_DONE],
        [],
        [],
        [],
    )
    assert sequencer.get_sequencer_status() == state


# ----------------------------------------------------------------------------
def test_sideband_calibration(sequencer, is_qrm_type):
    num_out = 1 if is_qrm_type else 2

    sequencer.parameters["connect_out0"]("IQ")
    if num_out == 2:
        sequencer.connect_out1("off")

    sequencer.nco_freq_cal_type_default("off")
    sequencer.nco_freq(100e6)
    sequencer.sideband_cal.assert_not_called()
    sequencer.sideband_cal.reset_mock()
    sequencer.nco_freq(100e6, cal_type="sideband")
    sequencer.sideband_cal.assert_called_once()
    sequencer.sideband_cal.reset_mock()

    sequencer.nco_freq_cal_type_default("sideband")
    sequencer.nco_freq(100e6)
    sequencer.sideband_cal.assert_called_once()
    sequencer.sideband_cal.reset_mock()
    sequencer.nco_freq(100e6, cal_type="off")
    sequencer.sideband_cal.assert_not_called()
    sequencer.sideband_cal.reset_mock()


# ----------------------------------------------------------------------------
def _assert_integration_values(measured_list, expected_list):
    assert len(measured_list) == len(expected_list)
    for measured_val, expected_val in zip(measured_list, expected_list):
        if isnan(expected_val):
            assert isnan(measured_val)
        else:
            assert measured_val == pytest.approx(expected_val, 1e-9)


# ----------------------------------------------------------------------------
def test_dummy_binned_acquisition(seq0, seq1, mod0, mod1, set_dummy_fun0, set_dummy_fun1):
    acquisitions = {"acq1": {"num_bins": 5, "index": 1}}
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": acquisitions,
        "program": "stop",
    }

    dummy_data0 = [
        DummyBinnedAcquisitionData(data=(0.1, 0.6), thres=1, avg_cnt=0),
        DummyBinnedAcquisitionData(data=(0.2, 0.7), thres=2, avg_cnt=11),
        DummyBinnedAcquisitionData(data=(0.3, 0.8), thres=3, avg_cnt=22),
        DummyBinnedAcquisitionData(data=(0.4, 0.9), thres=4, avg_cnt=33),
        DummyBinnedAcquisitionData(data=(0.5, 1.0), thres=5, avg_cnt=44),
    ]
    expected0 = {
        "acq1": {
            "index": 1,
            "acquisition": {
                "bins": {
                    "integration": {
                        "path0": [
                            0.10000002386515804,
                            0.20000000433911963,
                            0.29999999566088037,
                            0.4000000014463732,
                            0.5,
                        ],
                        "path1": [
                            0.5999999045393678,
                            0.6999999934913205,
                            0.7999999956608803,
                            0.8999999978304402,
                            1.0,
                        ],
                    },
                    "threshold": [1.0, 2.0, 3.0, 4.0, 5.0],
                    "avg_cnt": [0, 11, 22, 33, 44],
                }
            },
        }
    }
    dummy_data1 = [
        DummyBinnedAcquisitionData(data=(0.81, 0.95), thres=5, avg_cnt=44),
        DummyBinnedAcquisitionData(data=(0.82, 0.96), thres=6, avg_cnt=55),
        DummyBinnedAcquisitionData(data=(0.83, 0.97), thres=7, avg_cnt=66),
        DummyBinnedAcquisitionData(data=(0.84, 0.98), thres=8, avg_cnt=0),
        None,
    ]
    expected1 = {
        "acq1": {
            "index": 1,
            "acquisition": {
                "bins": {
                    "integration": {
                        "path0": [
                            0.810000001301736,
                            0.820000000433912,
                            0.8299999999276813,
                            0.8400001050066954,
                            float("nan"),
                        ],
                        "path1": [
                            0.9499999989152201,
                            0.9599999991321762,
                            0.9699999993491321,
                            0.9800000429572845,
                            float("nan"),
                        ],
                    },
                    "threshold": [5.0, 6.0, 7.0, 8.0, float("nan")],
                    "avg_cnt": [44, 55, 66, 0, 0],
                }
            },
        }
    }

    def _assert_acquisition(measured, expected):
        assert measured["acq1"]["index"] == expected["acq1"]["index"]
        _assert_integration_values(
            measured["acq1"]["acquisition"]["bins"]["integration"]["path0"],
            expected["acq1"]["acquisition"]["bins"]["integration"]["path0"],
        )
        _assert_integration_values(
            measured["acq1"]["acquisition"]["bins"]["integration"]["path1"],
            expected["acq1"]["acquisition"]["bins"]["integration"]["path1"],
        )
        _assert_integration_values(
            measured["acq1"]["acquisition"]["bins"]["threshold"],
            expected["acq1"]["acquisition"]["bins"]["threshold"],
        )
        _assert_integration_values(
            measured["acq1"]["acquisition"]["bins"]["avg_cnt"],
            expected["acq1"]["acquisition"]["bins"]["avg_cnt"],
        )

    sequencer0 = getattr(mod0, f"sequencer{seq0}")
    sequencer1 = getattr(mod1, f"sequencer{seq1}")

    sequencer0.sequence(sequence)
    sequencer1.sequence(sequence)

    set_dummy_fun0(acq_index_name="acq1", data=dummy_data0)
    set_dummy_fun1(acq_index_name="acq1", data=dummy_data1)

    sequencer0.start_sequencer()
    sequencer1.start_sequencer()

    measured0 = mod0.get_acquisitions(seq0)
    measured1 = mod1.get_acquisitions(seq1)

    _assert_acquisition(measured0, expected0)
    _assert_acquisition(measured1, expected1)


# ----------------------------------------------------------------------------
def test_dummy_scope_acquisition(seq, mod, set_dummy_fun, test_dataset_index):
    acquisitions = {"acq1": {"num_bins": 5, "index": 1}}
    sequence = {
        "waveforms": {},
        "weights": {},
        "acquisitions": acquisitions,
        "program": "stop",
    }

    dummy_data0 = DummyScopeAcquisitionData(
        data=[(0.1, 0.2), (0.3, 0.4), (0.5, 0.6), (0.7, 0.8), (0.9, 1.0)],
        out_of_range=(False, False),
        avg_cnt=(11, 22),
    )
    expected0 = {
        "acq1": {
            "index": 1,
            "acquisition": {
                "scope": {
                    "path0": {
                        "data": [
                            0.10001332326686505,
                            0.29999555891104496,
                            0.49997779455522495,
                            0.700004441088955,
                            0.899986676733135,
                        ],
                        "out-of-range": False,
                        "avg_cnt": 11,
                    },
                    "path1": {
                        "data": [
                            0.20000444108895502,
                            0.40000888217791003,
                            0.5999911178220899,
                            0.799995558911045,
                            1.0,
                        ],
                        "out-of-range": False,
                        "avg_cnt": 22,
                    },
                }
            },
        }
    }
    dummy_data1 = DummyScopeAcquisitionData(
        data=[(0.51, 0.52), (0.53, 0.54), (0.55, 0.56), (0.57, 0.58), (0.59, 1.50)],
        out_of_range=(False, False),
        avg_cnt=(33, 44),
    )
    expected1 = {
        "acq1": {
            "index": 1,
            "acquisition": {
                "scope": {
                    "path0": {
                        "data": [
                            0.5099998519637015,
                            0.5299995558911045,
                            0.5499992598185075,
                            0.5699989637459105,
                            0.5899986676733135,
                        ],
                        "out-of-range": False,
                        "avg_cnt": 33,
                    },
                    "path1": {
                        "data": [
                            0.5199960030199405,
                            0.5400031087622685,
                            0.559999111782209,
                            0.5799951148021495,
                            1.5,
                        ],
                        "out-of-range": False,
                        "avg_cnt": 44,
                    },
                }
            },
        }
    }

    dummy_data = [dummy_data0, dummy_data1]
    expected = [expected0, expected1]

    def _assert_acquisition(measured, expected):
        assert measured["acq1"]["index"] == expected["acq1"]["index"]
        _assert_integration_values(
            measured["acq1"]["acquisition"]["scope"]["path0"]["data"],
            expected["acq1"]["acquisition"]["scope"]["path0"]["data"],
        )
        _assert_integration_values(
            measured["acq1"]["acquisition"]["scope"]["path1"]["data"],
            expected["acq1"]["acquisition"]["scope"]["path1"]["data"],
        )
        assert (
            measured["acq1"]["acquisition"]["scope"]["path0"]["avg_cnt"]
            == expected["acq1"]["acquisition"]["scope"]["path0"]["avg_cnt"]
        )
        assert (
            measured["acq1"]["acquisition"]["scope"]["path0"]["out-of-range"]
            == expected["acq1"]["acquisition"]["scope"]["path0"]["out-of-range"]
        )
        assert (
            measured["acq1"]["acquisition"]["scope"]["path1"]["avg_cnt"]
            == expected["acq1"]["acquisition"]["scope"]["path1"]["avg_cnt"]
        )
        assert (
            measured["acq1"]["acquisition"]["scope"]["path1"]["out-of-range"]
            == expected["acq1"]["acquisition"]["scope"]["path1"]["out-of-range"]
        )

    set_dummy_fun(data=dummy_data[test_dataset_index])

    sequencer = getattr(mod, f"sequencer{seq}")
    sequencer.sequence(sequence)
    sequencer.start_sequencer()

    mod.store_scope_acquisition(seq, "acq1")

    measured = mod.get_acquisitions(seq)
    _assert_acquisition(measured, expected[test_dataset_index])


# ----------------------------------------------------------------------------
def test_feedback_sequencer_param(sequencer, is_qrm_type):
    """
    Tests feedback related sequencer handling function calls.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    is_qrm_type
        Sequencer parent is of type QRM
    """
    if is_qrm_type:
        sequencer.thresholded_acq_trigger_en(False)
        assert sequencer.thresholded_acq_trigger_en() is False
        sequencer.thresholded_acq_trigger_address(1)
        assert sequencer.thresholded_acq_trigger_address() == 1
        sequencer.thresholded_acq_trigger_invert(False)
        assert sequencer.thresholded_acq_trigger_invert() is False
        sequencer.thresholded_acq_marker_en(False)
        assert sequencer.thresholded_acq_marker_en() is False
        sequencer.thresholded_acq_marker_address(1)
        assert sequencer.thresholded_acq_marker_address() == 1
        sequencer.thresholded_acq_marker_invert(False)
        assert sequencer.thresholded_acq_marker_invert() is False
        with pytest.raises(ValueError):
            sequencer.thresholded_acq_trigger_address(17)
        with pytest.raises(ValueError):
            sequencer.thresholded_acq_marker_address(17)
        with pytest.raises(TypeError):
            sequencer.thresholded_acq_marker_en(3)

    # Check manual trigger counter settings
    for x in range(1, 16):
        sequencer.parameters[f"trigger{x}_count_threshold"].set(x)
        assert sequencer.parameters[f"trigger{x}_count_threshold"].get() == x
        sequencer.parameters[f"trigger{x}_threshold_invert"].set(True)
        assert sequencer.parameters[f"trigger{x}_threshold_invert"].get() is True

    # Check automated reset
    sequencer.reset_trigger_thresholding()
    for x in range(1, 16):
        assert sequencer.get_trigger_thresholding(x)[0] == 1
        assert sequencer.get_trigger_thresholding(x)[1] is False

    # Check automated trigger counter settings
    for x in range(1, 16):
        sequencer.set_trigger_thresholding(x, x, True)
        assert sequencer.get_trigger_thresholding(x)[0] == x
        assert sequencer.get_trigger_thresholding(x)[1] is True


# ----------------------------------------------------------------------------
def test_get_sequencer_status(sequencer):
    """
    Tests sequencer status interface.

    Parameters
    ----------
    sequencer
        Sequencer driver object
    """
    status = SequencerStatus(
        SequencerStatuses.OKAY,
        SequencerStates.IDLE,
        [],
        [],
        [],
        [],
    )
    assert status == sequencer.get_sequencer_status()

    sequencer.arm_sequencer()
    status = SequencerStatus(
        SequencerStatuses.OKAY,
        SequencerStates.ARMED,
        [],
        [],
        [],
        [],
    )
    print(sequencer.get_sequencer_status())
    assert status == sequencer.get_sequencer_status()

    sequencer.arm_sequencer()
    status = SequencerStatus(
        SequencerStatuses.OKAY,
        SequencerStates.STOPPED,
        [SequencerStatusFlags.ACQ_BINNING_DONE],
        [],
        [],
        [],
    )
    sequencer.start_sequencer()
    print(sequencer.get_sequencer_status())
