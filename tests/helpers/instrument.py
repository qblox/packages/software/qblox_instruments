# ----------------------------------------------------------------------------
# Description    : Generic test functions
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------


# -- include -----------------------------------------------------------------

import fastjsonschema

from qblox_instruments import InstrumentType


# ----------------------------------------------------------------------------
def test_reset_cache_invalidation(instrument):
    """
    Tests if the call to reset also invalidates the caches on the qcodes
    parameters.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    def cache_param(param):
        if param.name == "sequence":
            param({"waveforms": {}, "weights": {}, "acquisitions": {}, "program": "stop"})
        else:
            param()

    def assert_invalid(param):
        assert param.cache.valid is False, f"{param.name}"

    def parse_params(module, func):
        for param in module.parameters.values():
            func(param)

        for submodule in module.submodules.values():
            parse_params(submodule, func)

    # Get all params to make the cache valid
    parse_params(instrument, cache_param)

    # Reset instrument
    instrument.reset()

    # Check if all parameters are invalidated
    parse_params(instrument, assert_invalid)


# ----------------------------------------------------------------------------
def test_str(instrument, class_name, instrument_name):
    """
    Test string representation based in __str__

    Parameters
    ----------
    class_name
        Class name
    instrument_name
        Instrument name
    """

    assert str(instrument) == f"<{class_name}: {instrument_name}>"


# ----------------------------------------------------------------------------
def test_get_scpi_commands(instrument):
    """
    Tests get SCPI commands function call. If no exceptions occur and the
    returned object matches the json schema the test passes.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Get SCPI commands
    scpi_cmds = instrument._get_scpi_commands()

    # Check SCPI commands
    scpi_cmds_schema = {
        "title": "Command container.",
        "description": "Contains commands.",
        "type": "object",
        "required": [
            "scpi_in_type",
            "scpi_out_type",
            "python_func",
            "python_in_type",
            "python_in_var",
            "python_out_type",
            "comment",
        ],
        "properties": {
            "scpi_in_type": {
                "description": "SCPI input argument types",
                "type": "array",
            },
            "scpi_out_type": {"description": "SCPI output types", "type": "array"},
            "python_func": {"description": "Python function name", "type": "string"},
            "python_in_type": {
                "description": "Python function input types",
                "type": "array",
            },
            "python_in_var": {
                "description": "Python function input variable names",
                "type": "array",
            },
            "python_out_type": {
                "description": "Python function output types",
                "type": "array",
            },
            "comment": {"description": "Python function comment", "type": "string"},
        },
    }

    for scpi_cmd in scpi_cmds:
        validate_scpi_cmds = fastjsonschema.compile(scpi_cmds_schema)
        validate_scpi_cmds(scpi_cmds[scpi_cmd])


# ----------------------------------------------------------------------------
def test_get_idn(instrument):
    """
    Tests get IDN function call. If no exceptions occur and the returned
    object matches the json schema the test passes.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Get IDN
    idn = instrument.get_idn()

    # Check IDN
    idn_schema = {
        "title": "Device identification container.",
        "description": "Contains device identification.",
        "type": "object",
        "required": ["manufacturer", "model", "serial_number", "firmware"],
        "properties": {
            "manufacturer": {"description": "Manufacturer name", "type": "string"},
            "model": {"description": "Model name", "type": "string"},
            "serial_number": {"description": "Serial number", "type": "string"},
            "firmware": {
                "description": "Firmware build information",
                "type": "object",
                "required": ["fpga", "kernel_mod", "application", "driver"],
                "properties": {
                    "fpga": {
                        "description": "FPGA firmware build information",
                        "type": "object",
                    },
                    "kernel_mod": {
                        "description": "Kernel module build information",
                        "type": "object",
                    },
                    "application": {
                        "description": "Application build information",
                        "type": "object",
                    },
                    "driver": {
                        "description": "Driver build information",
                        "type": "object",
                    },
                },
            },
        },
    }

    build_schema = {
        "title": "Build information container.",
        "description": "Contains build information.",
        "type": "object",
        "required": ["version", "build", "hash", "dirty"],
        "properties": {
            "version": {"description": "Version", "type": "string"},
            "build": {"description": "Build date", "type": "string"},
            "hash": {"description": "Hash", "type": "string"},
            "dirty": {"description": "Dirty bit", "type": "boolean"},
        },
    }

    validate_idn = fastjsonschema.compile(idn_schema)
    validate_build = fastjsonschema.compile(build_schema)

    validate_idn(idn)
    validate_build(idn["firmware"]["fpga"])
    validate_build(idn["firmware"]["kernel_mod"])
    validate_build(idn["firmware"]["application"])
    validate_build(idn["firmware"]["driver"])


# ----------------------------------------------------------------------------
def test_scpi_commands(instrument):
    """
    Tests remaining mandatory SCPI commands. If no exceptions occur the
    test passes.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Functions without return value
    instrument.reset()
    instrument.clear()
    instrument.set_service_request_enable(0)
    instrument.set_standard_event_status_enable(0)
    instrument.set_operation_complete()
    instrument.wait()
    instrument.preset_system_status()
    instrument.set_questionable_enable(0)
    instrument.set_operation_enable(0)

    # Functions with return value (most return values are nonsense,
    # because of dummy instrument)
    assert instrument.get_status_byte() == 0
    assert instrument.get_service_request_enable() == 0
    assert instrument.get_standard_event_status_enable() == 0
    assert instrument.get_standard_event_status() == 0
    assert instrument.get_operation_complete() is False
    assert instrument.test() is False
    assert instrument.get_system_error() == "No error"
    assert instrument.get_num_system_error() == 0
    assert instrument.get_system_version() == "0"
    assert instrument.get_questionable_condition() == 0
    assert instrument.get_questionable_event() == 0
    assert instrument.get_questionable_enable() == 0
    assert instrument.get_operation_condition() == 0
    assert instrument.get_operation_events() == 0
    assert instrument.get_operation_enable() == 0


# ----------------------------------------------------------------------------
def test_get_temp(instrument):
    """
    Tests temperature readout function calls.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Return values are always 0.0, because of dummy instrument
    if instrument.instrument_type in (InstrumentType.QCM, InstrumentType.QRM):
        assert instrument.get_current_carrier_temperature() == 0.0
        assert instrument.get_maximum_carrier_temperature() == 0.0
        assert instrument.get_current_fpga_temperature() == 0.0
        assert instrument.get_maximum_fpga_temperature() == 0.0

        assert instrument.get_current_afe_temperature() == 0.0
        assert instrument.get_maximum_afe_temperature() == 0.0
        assert instrument.get_current_lo_temperature() == 0.0
        assert instrument.get_maximum_lo_temperature() == 0.0

    if instrument.instrument_type == InstrumentType.MM:
        assert instrument.get_current_carrier_temperature(1) == 0.0
        assert instrument.get_maximum_carrier_temperature(1) == 0.0
        assert instrument.get_current_fpga_temperature(1) == 0.0
        assert instrument.get_maximum_fpga_temperature(1) == 0.0

        assert instrument.get_current_afe_temperature(1) == 0.0
        assert instrument.get_maximum_afe_temperature(1) == 0.0
        assert instrument.get_current_lo_temperature(1) == 0.0
        assert instrument.get_maximum_lo_temperature(1) == 0.0

        assert instrument.get_current_bp_temperature_0() == 0.0
        assert instrument.get_maximum_bp_temperature_0() == 0.0
        assert instrument.get_current_bp_temperature_1() == 0.0
        assert instrument.get_maximum_bp_temperature_1() == 0.0
        assert instrument.get_current_bp_temperature_2() == 0.0
        assert instrument.get_maximum_bp_temperature_2() == 0.0


# ----------------------------------------------------------------------------
def test_identify(instrument):
    """
    Tests test identify function calls.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    instrument.identify()


# ----------------------------------------------------------------------------
def test_led_brightness(instrument):
    """
    Tests LED brightness setting and getting function calls.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Set all possible inputs
    instrument.led_brightness("off")
    instrument.led_brightness("low")
    instrument.led_brightness("medium")
    instrument.led_brightness("high")

    # Set invalid input
    try:
        instrument.led_brightness("random")
        raise AssertionError("Setting LED brightness to random should have failed.")
    except ValueError:
        pass

    # Dummy nonsense answer always returns "high"
    assert instrument.led_brightness() == "high"


# ----------------------------------------------------------------------------
def test_ref_src(instrument):
    """
    Tests reference source setting and getting function calls.

    Parameters
    ----------
    instrument
        Instrument driver object
    """

    # Set all possible inputs
    instrument.reference_source("internal")
    instrument.reference_source("external")

    # Set invalid input
    try:
        instrument.reference_source("random")
        raise AssertionError("Setting reference source to random should have failed.")
    except ValueError:
        pass

    # Dummy nonsense answer always returns "external"
    assert instrument.reference_source() == "external"
