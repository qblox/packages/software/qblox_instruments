# ----------------------------------------------------------------------------
# Description    : QCM/QRM/QRC test functions
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------
import pytest


# ----------------------------------------------------------------------------
def test_sequencer_access(qcm_qrx):
    """
    Tests if sequencers can be accessed.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    if qcm_qrx.is_qcm_type or qcm_qrx.is_qrm_type or qcm_qrx.is_qrc_type:
        assert len(qcm_qrx.sequencers) == 6
    for seq_idx, sequencer in enumerate(qcm_qrx.sequencers):
        assert sequencer.name == f"{qcm_qrx.name}_sequencer{seq_idx}"
        assert sequencer.name == qcm_qrx[f"sequencer{seq_idx}"].name


# ----------------------------------------------------------------------------
def test_lo_freq(qcm_qrx):
    """
    Tests QCM LO frequency setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    if qcm_qrx.is_qcm_type:
        num_out = 2
        in_str = ""
    else:
        num_out = 1
        in_str = "_in0"

    for lo_idx in range(0, num_out):
        param_name = f"out{lo_idx}{in_str}_lo_freq"

        # Check LO frequency function call
        qcm_qrx.parameters[param_name].set(10e9)

        # Check LO frequency invalid values
        try:
            qcm_qrx.parameters[param_name].set(1.9e9)
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass

        # Check LO frequency invalid values
        try:
            qcm_qrx.parameters[param_name].set(18.1e9)
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass

        # Check function calls
        qcm_qrx.parameters[param_name].set(2e9)
        assert qcm_qrx.parameters[param_name].get() == 2e9


# ----------------------------------------------------------------------------
def test_lo_mixer_cal(qcm_qrx):
    if qcm_qrx.is_qcm_type:
        num_out = 2
        in_str = ""
    else:
        num_out = 1
        in_str = "_in0"

    for seq in qcm_qrx.sequencers:
        seq.parameters["connect_out0"]("IQ")
        if num_out == 2:
            seq.connect_out1("off")

    # Check LO mixer calibration calls
    for lo_idx in range(0, num_out):
        param_name = f"out{lo_idx}{in_str}_lo_freq"
        qcm_qrx.parameters[f"out{lo_idx}{in_str}_lo_freq_cal_type_default"].set("off")
        qcm_qrx.parameters[param_name](5e9)
        qcm_qrx._run_mixer_lo_calib.assert_not_called()
        qcm_qrx._run_mixer_lo_calib.reset_mock()

        qcm_qrx.parameters[param_name](5e9, cal_type="lo only")
        qcm_qrx._run_mixer_lo_calib.assert_called_once()
        qcm_qrx._run_mixer_lo_calib.reset_mock()

        qcm_qrx.parameters[f"out{lo_idx}{in_str}_lo_freq_cal_type_default"].set("lo only")
        qcm_qrx.parameters[param_name](5e9)
        qcm_qrx._run_mixer_lo_calib.assert_called_once()
        qcm_qrx._run_mixer_lo_calib.reset_mock()

        qcm_qrx.parameters[param_name](5e9, cal_type="off")
        qcm_qrx._run_mixer_lo_calib.assert_not_called()
        qcm_qrx._run_mixer_lo_calib.reset_mock()

        qcm_qrx.parameters[f"out{lo_idx}{in_str}_lo_freq_cal_type_default"].set("lo and sidebands")
        qcm_qrx.parameters[param_name](5e9)
        for seq in qcm_qrx.sequencers:
            print(seq.sideband_cal)
            if lo_idx:  # if output is the connected one.
                seq.sideband_cal.assert_not_called()
            else:  # if output is the disconnected one.
                seq.sideband_cal.assert_called_once()
            seq.sideband_cal.reset_mock()
        qcm_qrx._run_mixer_lo_calib.assert_called_once()
        qcm_qrx._run_mixer_lo_calib.reset_mock()


# ----------------------------------------------------------------------------
def test_lo_enable(qcm_qrx):
    """
    Tests LO enable setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    if qcm_qrx.is_qcm_type:
        num_out = 2
        in_str = ""
    else:
        num_out = 1
        in_str = "_in0"

    for lo_idx in range(0, num_out):
        param_name = f"out{lo_idx}{in_str}_lo_en"

        # Check function calls
        qcm_qrx.parameters[param_name].set(True)
        assert qcm_qrx.parameters[param_name].get() is True


# ----------------------------------------------------------------------------
def test_lo_pwr(qcm_qrx):
    """
    Tests LO power setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    # Check LO power function calls
    for lo_idx in range(0, 2):
        getattr(qcm_qrx, f"_set_lo_pwr_{lo_idx}")(10)
        assert getattr(qcm_qrx, f"_get_lo_pwr_{lo_idx}")() == 10


# ----------------------------------------------------------------------------
def test_in_amp_gain(qcm_qrx):
    """
    Tests input amplifier gain setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    for idx in range(0, 2):
        # Check input amplifier gain function calls
        qcm_qrx.parameters[f"in{idx}_gain"].set(0)

        # Check invalid gain settings
        try:
            qcm_qrx.parameters[f"in{idx}_gain"].set(-7)
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass

        try:
            qcm_qrx.parameters[f"in{idx}_gain"].set(27)
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass

        # Check function calls
        qcm_qrx.parameters[f"in{idx}_gain"].set(20)
        assert qcm_qrx.parameters[f"in{idx}_gain"].get() == 20


# ----------------------------------------------------------------------------
def test_out_amp_offset(qcm_qrx, num_out):
    """
    Tests output amplifier offset setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    num_out
        Number of outputs
    """

    for out_idx in range(0, num_out):
        for path_idx in range(0, 2):
            # Check output offset function calls
            qcm_qrx.parameters[f"out{out_idx}_offset_path{path_idx}"].set(0)

            # Check offset settings
            try:
                qcm_qrx.parameters[f"out{out_idx}_offset_path{path_idx}"].set(-85.0)
                raise AssertionError("Setting value out of bounds should have failed.")
            except ValueError:
                pass

            try:
                qcm_qrx.parameters[f"out{out_idx}_offset_path{path_idx}"].set(74.0)
                raise AssertionError("Setting value out of bounds should have failed.")
            except ValueError:
                pass

            # Check function calls
            qcm_qrx.parameters[f"out{out_idx}_offset_path{path_idx}"].set(27)
            assert qcm_qrx.parameters[f"out{out_idx}_offset_path{path_idx}"].get() == 27


# ----------------------------------------------------------------------------
def test_out_dac_offset(qcm_qrx, num_dacs, max_v):
    """
    Tests output DAC offset setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    num_dacs
        Number of DACs
    max_v
        Maximum voltage
    """

    for idx in range(0, num_dacs):
        # Check output offset function calls
        qcm_qrx.parameters[f"out{idx}_offset"].set(0)

        # Check offset settings
        try:
            qcm_qrx.parameters[f"out{idx}_offset"].set(-1 * (max_v + 0.1))
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass
        try:
            qcm_qrx.parameters[f"out{idx}_offset"].set(max_v + 0.1)
            raise AssertionError("Setting value out of bounds should have failed.")
        except ValueError:
            pass
        # Check function calls
        qcm_qrx.parameters[f"out{idx}_offset"].set(max_v - 0.1)
        assert qcm_qrx.parameters[f"out{idx}_offset"].get() == max_v - 0.1


# ----------------------------------------------------------------------------
def test_attenuation(qcm_qrx, in_out, num_att, max_dB):
    """
    Tests input/output attenuation setting and getting function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    in_out
        Whether it's input or output attenuation. True: Input, False: Output
    num_att
        Number of attenuators for that stage
    max_dB
        Maximum attenuation
    """
    for idx in range(0, num_att):
        # Check defaults
        if in_out:
            assert qcm_qrx.parameters[f"in{idx}_att"].get() == 0
        else:
            assert qcm_qrx.parameters[f"out{idx}_att"].get() == 0

        # Check attenuation function calls. Change away from 0 to make sure setting works
        if in_out:
            qcm_qrx.parameters[f"in{idx}_att"].set(2)
            assert qcm_qrx.parameters[f"in{idx}_att"].get() == 2
        else:
            qcm_qrx.parameters[f"out{idx}_att"].set(2)
            assert qcm_qrx.parameters[f"out{idx}_att"].get() == 2

        # Check attenuation settings
        with pytest.raises(ValueError):
            if in_out:
                qcm_qrx.parameters[f"in{idx}_att"].set(-1)
            else:
                qcm_qrx.parameters[f"out{idx}_att"].set(-1)

        with pytest.raises(ValueError):
            if in_out:
                qcm_qrx.parameters[f"in{idx}_att"].set(max_dB + 2)
            else:
                qcm_qrx.parameters[f"out{idx}_att"].set(max_dB + 2)
    with pytest.raises(KeyError):
        qcm_qrx.parameters[f"in{num_att}_att"].get()
        qcm_qrx.parameters[f"out{num_att}_att"].get()


# ----------------------------------------------------------------------------
def test_scope_acquisition_control(qcm_qrx):
    """
    Tests scope acquisition control function calls.

    Parameters
    ----------
    qcm_qrx
        QCM/QRM/QRC driver object
    """

    qcm_qrx.scope_acq_sequencer_select(1)
    assert qcm_qrx.scope_acq_sequencer_select() == 1
