# ----------------------------------------------------------------------------
# Description    : QTM test functions
# Git repository : https://gitlab.com/qblox/packages/software/qblox_instruments.git
# Copyright (C) Qblox BV (2020)
# ----------------------------------------------------------------------------


# ----------------------------------------------------------------------------
def test_io_channel_access(qtm):
    """
    Tests if IO channels can be accessed.

    Parameters
    ----------
    qcm_qrx
        QTM driver object
    """

    if qtm.is_qtm_type:
        assert len(qtm.io_channels) == 8
        assert len(qtm.quads) == 2
        assert not hasattr(qtm, "io_pulse_channel.pulse_width")
        assert not hasattr(qtm, "io_pulse_channel.output_offset")
        assert not hasattr(qtm, "io_pulse_channel.output_normalized_amplitude")
    for dio_idx, io_channel in enumerate(qtm.io_channels):
        assert io_channel.name == f"{qtm.name}_io_channel{dio_idx}"
        assert io_channel.name == qtm[f"io_channel{dio_idx}"].name
    for quad_idx, quad in enumerate(qtm.quads):
        assert quad.name == f"{qtm.name}_quad{quad_idx}"
        assert quad.name == qtm[f"quad{quad_idx}"].name


# ----------------------------------------------------------------------------
def test_pulse_access(qtm):
    """
    Tests if IO channels can be accessed.

    Parameters
    ----------
    qtm
        QTM driver object

    Returns
    ----------

    Raises
    ----------
    """

    if qtm.is_qtm_type and qtm.is_eom_type:
        assert hasattr(qtm, "io_pulse_channel.pulse_width")
        assert hasattr(qtm, "io_pulse_channel.output_offset")
        assert hasattr(qtm, "io_pulse_channel.output_normalized_amplitude")
