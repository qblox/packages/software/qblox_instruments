from qblox_instruments.pnp.main import PNP_PORT, PlugAndPlay
from qblox_instruments.pnp.resolve import CMM_SLOT_INDEX, AddressInfo, resolve
